from django.http import HttpResponse, Http404
from django.views.generic import CreateView, ListView, DetailView
from django.core import serializers

from .models import OfertasModel, CategoriasModel
from .forms import OfertasForm


class OfertasCreateView(CreateView):

    form_class = OfertasForm

    template_name = 'ofertas/ofertas_create.html'

    success_url = '/'


class OfertasListView(ListView):

    template_name = 'ofertas/ofertas_list.html'

    context_object_name = 'ofertas'

    #Numero de Ofertas por Pagina
    paginate_by = 10

    def get_queryset(self):
        """ Retorna todas las ofertas en orden inverso
        la 'primera' es la Ultima Oferta publicada """

        return OfertasModel.objects.all()[::-1]

    def get_context_data(self, **kwargs):
        """ para poder listar todas las categorias ( para el filtro ) en el
        mismo template"""
        context = super(OfertasListView, self).get_context_data(**kwargs)
        context['categorias'] = [categoria for categoria in
                                 CategoriasModel.objects.all()]
        return context


class OfertaDetailView(DetailView):

    model = OfertasModel

    template_name = 'ofertas/ofertas_detail.html'

    context_object_name = 'oferta'

    def get_context_data(self, **kwargs):
        context = super(OfertaDetailView, self).get_context_data(**kwargs)
        """ contexto para mostrar las ofertas de la misma categoria de la
        oferta que se esta viendo solo se muestra las ultimas 5"""

        context['similares'] =(
                OfertasModel.objects.filter(
                    categoria=context['object'].categoria).exclude(
                    pk=context['object'].pk)[:5:-1]
            )


        return context


def logic_ajax(request):
    """ Logica para el ajax, solo funciona si es una
        peticion hecha con ajax, de lo contrario
        devuelve un error 404 'pagina no encontrada' """

    if request.is_ajax():
        """ Recogemos el id que nos esta pasando el ajax """
        cat = CategoriasModel.objects.get(id=request.GET['idcategoria'])

        """ Filtramos todas las ofertas que tengan esa categoria """
        ofertas = OfertasModel.objects.filter(categoria=cat)

        """ Ya que ajax no entiende los datos de Python necesitamos
        serializar los datos a JSON para eso se importa los serializers"""
        data = serializers.serialize('json', ofertas,
                                     fields={'pk', 'titulo', 'slug'})

        return HttpResponse(data, content_type='application/json')

    else:
        raise Http404

