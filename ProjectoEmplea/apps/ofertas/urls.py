from django.conf.urls import patterns, url

from .views import OfertasCreateView, OfertasListView, OfertaDetailView

urlpatterns = patterns('',
    url(r'^$', OfertasListView.as_view()),
    url(r'^new/jobs/$', OfertasCreateView.as_view(),name='nueva_oferta'),

    url(r'^oferta/(?P<slug>[-\w]+)/(?P<pk>\d+)/$',
        OfertaDetailView.as_view(), name='listar'),

    url(r'^ajax/$', 'apps.ofertas.views.logic_ajax'),

)
