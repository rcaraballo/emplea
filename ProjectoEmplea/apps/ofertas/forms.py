#encoding:utf-8
from django import forms

from captcha.fields import ReCaptchaField
from .models import OfertasModel, codigo

class OfertasForm(forms.ModelForm):
    titulo = forms.CharField(widget=forms.TextInput(attrs={
					'class' : 'large columns',
                    #'placeholder' : 'Se solicita Programador WEB'
            	    }))

    empresa = forms.CharField(widget=forms.TextInput(attrs={
                    'class' : 'large columns',
                    #'placeholder' : 'Nombre de la Empresa'
                    }))

    url = forms.URLField(required=False, widget=forms.TextInput(attrs={
    				'class' : 'small columns',
    				#'placeholder' : 'http://www.tu-empresa.com/'
                    }))

    lugar = forms.CharField(widget=forms.TextInput(attrs={
                    'class' : 'large-12 columns',
                    'placeholder' : 'Santo Domingo / Santiago / La Romana ...'
                     }))

    descripcion = forms.CharField(widget=forms.Textarea(attrs={
                'class':'large-12 columns', 'id' : 'editor'
                }))

    comoAplicar = forms.CharField(widget=forms.Textarea(attrs={
                    'class':'large-12 columns',
                    }))

    codigo = codigo()

    captcha = ReCaptchaField(attrs={'theme':'clean'})

    class Meta:
        model = OfertasModel
