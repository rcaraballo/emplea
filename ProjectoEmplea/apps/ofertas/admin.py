from django.contrib import admin

from .models import OfertasModel, CategoriasModel

class OfertasModelAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'categoria', 'publicado', 'activo')
    list_filter = ('categoria', 'publicado', 'activo')

admin.site.register(OfertasModel, OfertasModelAdmin)
admin.site.register(CategoriasModel)