import random

from django.db import models
from django.template.defaultfilters import slugify



class CategoriasModel(models.Model):

    nombre = models.CharField(max_length=25)

    def __unicode__(self):
        return self.nombre

def codigo():
    character = ['0','1','2','3','4','5','6','7','8','9',
                'a','b','c','d','e','f','g','h','i','j','k','l','m',
                'n','o','p','q','r','s','t','u','v','w','x','y','z']
    lenCode = 8
    code =''
    i = 0
    while i <= lenCode:
        code += random.choice(character)
        i += 1

    return code

class OfertasModel(models.Model):

    titulo = models.CharField(max_length=50)
    empresa = models.CharField(max_length=25)
    url = models.URLField(null=True, blank=True)
    descripcion = models.TextField()
    lugar = models.CharField(max_length=50)
    comoAplicar = models.TextField()
    publicado = models.DateTimeField(auto_now=True, auto_now_add=True)
    codigo = models.CharField(max_length=9, default=codigo, editable=False)
    slug = models.SlugField(editable=False, unique=False)
    categoria = models.ForeignKey(CategoriasModel)
    activo = models.BooleanField(default=False)


    def __unicode__(self):
        return self.titulo 

    def save(self, *args, **kargs):
        '''Crea el slug de la oferta usando su titulo'''
        if not self.id:
            self.slug = slugify(self.titulo)
        super(OfertasModel, self).save(*args, **kargs)