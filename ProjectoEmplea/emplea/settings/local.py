from .base import *

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR.child('DB.sqlite3'),
    }
}

STATIC_URL = '/static/'

STATIC_ROOT = BASE_DIR.child('rootStatic')


STATICFILES_DIRS = (
    BASE_DIR.child('static'),
    )

TEMPLATE_DIRS = (
    BASE_DIR.child('templates'),
    )

#configuracion para el django-wysiwyg
DJANGO_WYSIWYG_FLAVOR = "ckeditor"


#configuracion para el CKEDITOR#
CKEDITOR_UPLOAD_PATH = "uploads/"
