from unipath import Path
BASE_DIR = Path(__file__).ancestor(3)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '2tnfw)+6_^j5cvkrf#jxm@@0$f1o_iei4d-#(z@=xht-qdx9w+'

# Application definition

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    )

THIRD_PARTY_APPS = (
    'django_wysiwyg',
    'ckeditor',
    'captcha',

    )

LOCAL_APPS = (
    'apps.ofertas',

    )


INSTALLED_APPS = (
    DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'emplea.urls'

WSGI_APPLICATION = 'emplea.wsgi.application'

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Santo_Domingo'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# KEYS para ReCaptcha
RECAPTCHA_PUBLIC_KEY = '6LfzTfYSAAAAAAJ0r1XKQaJkYQWSQ1uuuqdQqISX'
RECAPTCHA_PRIVATE_KEY = '6LfzTfYSAAAAAN1O31ozI0hSxoanytqqccNHpnvV'